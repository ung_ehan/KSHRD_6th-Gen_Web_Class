$(document).ready(function(){

    // HIDE, SHOW & TOGGLE
    $("#hide").click(function(){
        $("#wrapper").hide("slow",function(){
            // alert("Hide Finished!")
        })
    })      
    
    $("#show").click(function(){
        var div = $("#wrapper");
        div.show("fast")
    })

    $("#tog").click(function(){
        var div = $("#wrapper");
        div.toggle(1000)
    })

    // FADE 
    $("#fdout").click(function(){
        var div = $("#wrapper");
        div.fadeOut(1000)
    }) 
    $("#fdin").click(function(){
        var div = $("#wrapper");
        div.fadeIn(1000)
    }) 
    $("#fdtog").click(function(){
        var div = $("#wrapper");
        div.fadeToggle(1000)
    })
    $("#fdto").click(function(){
        var div = $("#wrapper");
        div.fadeTo(1000,0.5)
    })

    // SLIDING
    $("#sl-up").click(function(){
        $("#wrapper").slideUp(1000);
    })

    $("#sl-down").click(function(){
        $("#wrapper").slideDown(1000);
    })

    $("#sl-tog").click(function(){
        $("#wrapper").slideToggle(1000);
    })

    // Animation
    $("#ani1").click(function(){
        $("#wrapper")
            .animate({width:'100%'},5000)
            .animate({height:'300px'},5000);
    })

    $("#ani2").click(function(){
        $("#wrapper")
            .animate({width:'500px'},5000)
            .animate({height:'200px'},5000);
    })

    $("#stop").click(function(){
        $("#wrapper").stop(true,true)
    })


});